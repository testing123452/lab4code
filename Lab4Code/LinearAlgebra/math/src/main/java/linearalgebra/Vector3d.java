// Jaypee Tello || 2232311
package linearalgebra;

public class Vector3d {
    private double x;
    private double y;
    private double z;

    public Vector3d (double newX, double newY, double newZ) {
        this.x = newX;
        this.y = newY;
        this.z = newZ;
    }

    public double getX() {
        return this.x;
    }
    
    public double getY() {
        return this.y;
    }

    public double getZ() {
        return this.z;
    }

    public double magnitude() {
        return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2) + Math.pow(z, 2));
    }

    public double dotProduct(Vector3d Vector3dInput) {
        double dotX = Vector3dInput.getX();
        double dotY = Vector3dInput.getY();
        double dotZ = Vector3dInput.getZ();

        return ((this.x * dotX) + (this.y * dotY) + (this.z * dotZ));
    }

    public Vector3d add(Vector3d Vector3dInput) {
        double inputX = Vector3dInput.getX();
        double inputY = Vector3dInput.getY();
        double inputZ = Vector3dInput.getZ();

        double newX = this.x  + inputX;
        double newY = this.y + inputY;
        double newZ = this.z + inputZ;

        Vector3d newVector3d = new Vector3d(newX, newY, newZ);

        return newVector3d;
    }

    public String toString() {
        return ("(" + this.x + "," + this.y + "," + this.z + ")");
    }


}
